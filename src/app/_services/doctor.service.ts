import { Injectable } from '@angular/core';
import { IDoctor, DocotrDto } from '../DTO';
import { BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AppContextService } from './app-context.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DoctorService {
  private doctors = new BehaviorSubject<IDoctor[]>(null);
  doctors$ = this.doctors.asObservable();

  constructor(
    private http: HttpClient,
    private appContextService: AppContextService
  ) {}

  getDoctors() {
    this.appContextService.setProgressbarValue(true);
    this.http
      .get<DocotrDto>(`${environment.apiOrigin}/doctors`)
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          throw err;
        }),
        map((res) => {
          return res.results;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        this.doctors.next(res);
      });
  }
}

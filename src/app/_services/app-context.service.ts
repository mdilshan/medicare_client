import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppContextService {
  configLoader = new BehaviorSubject<number>(0);
  private loadingProgressbar = new BehaviorSubject<boolean>(false);
  public LastPrograssbarValue = false;

  constructor(public snackBar: MatSnackBar, private router: Router) {
    this.loadingProgressbar.pipe(debounceTime(100)).subscribe(v => {
      this.LastPrograssbarValue = v;
    });
  }

  public showSnackBarListern(
    text: string,
    action: string,
    duration: number
  ): Observable<any> {
    const obsrvbl = new Observable(observer => {
      let snackBarRef = this.snackBar.open(text, action, {
        duration: duration
      });
      snackBarRef.onAction().subscribe(() => {
        observer.next(true);
        observer.complete();
      });
      setTimeout(() => {
        observer.next(false);
        observer.complete();
      }, duration);
    });
    return obsrvbl;
  }

  public showSnackBarShow(
    text: string,
    action: string = 'ERROR',
    duration: number = 4000,
    panelClass: string = ''
  ): void {
    this.snackBar.open(text, action, {
      duration: duration,
      panelClass: panelClass
    });
  }

  public handleHTTPErrors(
    error: any,
    show_snackbar: boolean = true,
    action: string = 'ERROR',
    panelClass: string = ''
  ): string {
    let msg = '';
    if (environment.error_logging) {
      console.error(error);
    }
    if (error.status == 401 || error.status == 403) {
      location.href = './login';
      return;
    }
    if (typeof error == 'string') {
      msg = error;
    } else if (error.error && error.error.message) {
      msg = error.error.message;
    } else {
      msg = 'Unexpacted';
    }
    if (show_snackbar) {
      this.showSnackBarShow(msg, action, 2000, panelClass);
    }
    if (msg == 'Post not Found') {
      this.router.navigate(['PostNotFound']);
    }
    return msg;
  }

  public get loadingValue(): boolean {
    return this.LastPrograssbarValue;
  }
  setProgressbarValue(v: boolean) {
    this.loadingProgressbar.next(v);
  }
}

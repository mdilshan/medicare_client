import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ICustomer, CustomerDto } from '../DTO';
import { HttpClient } from '@angular/common/http';
import { AppContextService } from './app-context.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private customers = new BehaviorSubject<ICustomer[]>(null);
  customers$ = this.customers.asObservable();

  constructor(
    private http: HttpClient,
    private appContextService: AppContextService
  ) {}

  getCustomers() {
    this.appContextService.setProgressbarValue(true);
    this.http
      .get<CustomerDto>(`${environment.apiOrigin}/customers`)
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          throw err;
        }),
        map((res) => {
          return res.results;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        this.customers.next(res);
      });
  }
}

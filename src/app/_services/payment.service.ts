import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppContextService } from './app-context.service';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PaymentDto, IPayment, FullPaymanetDetailsDTO } from '../DTO';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  private payments = new BehaviorSubject<IPayment[]>(null);
  payments$ = this.payments.asObservable();

  constructor(
    private http: HttpClient,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  getPayments(limit: number, skip: number) {
    this.appContextService.setProgressbarValue(true);
    this.http
      .get<PaymentDto>(
        `${environment.apiOrigin}/payment?limit=${limit}&skip=${skip}`
      )
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          this.appContextService.showSnackBarShow(err);
          throw err;
        }),
        map((res) => {
          return res.results;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        if (res && res.length > 0) {
          this.payments.next(res);
        }
      });
  }

  addPayment(data: any) {
    this.appContextService.setProgressbarValue(true);
    this.http
      .post(`${environment.apiOrigin}/payment`, data)
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          this.appContextService.showSnackBarShow(err);
          throw err;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        this.appContextService.showSnackBarShow(
          'Payment Added',
          'SUCCESS',
          40000,
          'snackbar-green'
        );
        this.router.navigate(['/']);
      });
  }

  updatePayment(data: any, id: number) {
    this.appContextService.setProgressbarValue(true);
    this.http
      .put(`${environment.apiOrigin}/payment/${id}`, data)
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          this.appContextService.showSnackBarShow(err);
          throw err;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        this.appContextService.showSnackBarShow(
          'Payment Updated',
          'SUCCESS',
          40000,
          'snackbar-green'
        );
        this.router.navigate([`/show/${id}`]);
      });
  }

  deletePayment(id: number) {
    this.appContextService.setProgressbarValue(true);
    this.http
      .delete(`${environment.apiOrigin}/payment/${id}`)
      .pipe(
        catchError((err) => {
          this.appContextService.setProgressbarValue(false);
          this.appContextService.showSnackBarShow(err);
          throw err;
        })
      )
      .subscribe((res) => {
        this.appContextService.setProgressbarValue(false);
        this.appContextService.showSnackBarShow(
          'Payment Deleted',
          'SUCCESS',
          40000,
          'snackbar-green'
        );
        this.router.navigate([`/`]);
      });
  }

  async getOnePayment(id: number) {
    const res = await this.http
      .get<FullPaymanetDetailsDTO>(`${environment.apiOrigin}/payment/${id}`)
      .toPromise();

    return res;
  }
}

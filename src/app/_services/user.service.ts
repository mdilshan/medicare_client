import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth-service.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  registeUser(
    email: string,
    accountPassword: string,
    phone: string
  ): Promise<any> {
    return this.http
      .post(environment.apiOrigin + '/auth/login', {})
      .toPromise();
  }

  loginUser(data: any): Promise<any> {
    return this.http
      .post(environment.apiOrigin + '/auth/login', data)
      .toPromise();
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

export const enum UserAuthState {
  default = 0,
  userLoggedIn = 1,
  guest = 2,
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authToken = new BehaviorSubject<string>(null);
  authToken$ = this.authToken.asObservable();

  authState = new BehaviorSubject<number>(UserAuthState.default);

  constructor(private http: HttpClient, private router: Router) {}

  async saveUsersLocalStorage(token: string) {
    await localStorage.setItem('token', token);
  }

  getToken() {
    return this.authToken.value;
  }

  async authCheck(): Promise<boolean> {
    try {
      const storedToken = await localStorage.getItem('token');

      if (!storedToken) {
        return false;
      } else {
        this.authToken.next(storedToken);
        this.authState.next(UserAuthState.userLoggedIn);
        return true;
      }
    } catch (err) {
      throw err;
    }
  }
}

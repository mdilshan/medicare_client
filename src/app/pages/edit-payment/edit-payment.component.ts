import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDoctor, ICustomer, IFullPayment } from 'src/app/DTO';
import { Subscription } from 'rxjs';
import { DoctorService } from 'src/app/_services/doctor.service';
import { CustomerService } from 'src/app/_services/customers.service';
import { PaymentService } from 'src/app/_services/payment.service';
import { AppContextService } from 'src/app/_services/app-context.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-payment',
  templateUrl: './edit-payment.component.html',
  styleUrls: ['./edit-payment.component.scss'],
})
export class EditPaymentComponent implements OnInit, OnDestroy {
  service = [
    'Clinical Services',
    'Cancer Services',
    'Heart/Cardiovascular Services',
    "Men's Health Services",
    'Women’s Health Services',
    'Weight Management Services',
  ];
  type = ['ICU', 'Channel', 'Admitted'];
  doctors: IDoctor[] = [];
  customers: ICustomer[] = [];
  sub1: Subscription;
  sub2: Subscription;
  data: IFullPayment;
  constructor(
    private doctorsService: DoctorService,
    private customerService: CustomerService,
    private paymentService: PaymentService,
    private appContextService: AppContextService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  formGroup: FormGroup;

  ngOnInit(): void {
    const id = this.route.snapshot.params['payment_id'];
    if (id) {
      this.init(id);
    } else {
      this.router.navigate(['/']);
    }

    this.sub1 = this.doctorsService.doctors$.subscribe((val) => {
      this.doctors = val;
    });

    this.sub2 = this.customerService.customers$.subscribe((val) => {
      this.customers = val;
    });

    this.doctorsService.getDoctors();
    this.customerService.getCustomers();
  }

  async init(id: number) {
    try {
      this.appContextService.setProgressbarValue(true);
      const res = await this.paymentService.getOnePayment(id);
      this.data = res.results[0];
      this.formGroup = new FormGroup({
        cust_id: new FormControl(this.data.cust_id, [Validators.required]),
        dct_id: new FormControl(this.data.doct_id, [Validators.required]),
        service: new FormControl(this.data.service, [Validators.required]),
        cost: new FormControl(this.data.cost, [
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        ]),
        discount: new FormControl(this.data.discount, [
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        ]),
        type: new FormControl(this.data.type, [Validators.required]),
      });
    } catch (err) {
      this.appContextService.showSnackBarShow(err.message || err);
    } finally {
      this.appContextService.setProgressbarValue(false);
    }
  }

  onSubmit() {
    if (!this.formGroup.valid) {
      this.appContextService.showSnackBarShow('Invalid Form');
      return;
    }
    const payload = this.formGroup.value;
    // get random id
    const id = Math.floor(Math.random() * 10);
    payload.id = id;
    this.paymentService.updatePayment(payload, this.data.payment_id);
  }

  cancel() {
    this.router.navigate([`/show/${this.data.payment_id}`]);
  }

  ngOnDestroy() {
    this.sub2.unsubscribe();
    this.sub1.unsubscribe();
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDoctor, ICustomer } from 'src/app/DTO';
import { DoctorService } from 'src/app/_services/doctor.service';
import { CustomerService } from 'src/app/_services/customers.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppContextService } from 'src/app/_services/app-context.service';
import { PaymentService } from 'src/app/_services/payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss'],
})
export class PaymentFormComponent implements OnInit, OnDestroy {
  service = [
    'Clinical Services',
    'Cancer Services',
    'Heart/Cardiovascular Services',
    "Men's Health Services",
    'Women’s Health Services',
    'Weight Management Services',
  ];
  type = ['ICU', 'Channel', 'Admitted'];
  doctors: IDoctor[] = [];
  customers: ICustomer[] = [];
  sub1: Subscription;
  sub2: Subscription;

  formGroup = new FormGroup({
    cust_id: new FormControl(null, [Validators.required]),
    dct_id: new FormControl(null, [Validators.required]),
    service: new FormControl(null, [Validators.required]),
    cost: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^-?(0|[1-9]\d*)?$/),
    ]),
    discount: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^-?(0|[1-9]\d*)?$/),
    ]),
    type: new FormControl(null, [Validators.required]),
  });

  constructor(
    private doctorsService: DoctorService,
    private customerService: CustomerService,
    private paymentService: PaymentService,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sub1 = this.doctorsService.doctors$.subscribe((val) => {
      this.doctors = val;
    });

    this.sub2 = this.customerService.customers$.subscribe((val) => {
      this.customers = val;
    });

    this.doctorsService.getDoctors();
    this.customerService.getCustomers();
  }

  onSubmit() {
    if (!this.formGroup.valid) {
      this.appContextService.showSnackBarShow('Invalid Form');
      return;
    }
    const payload = this.formGroup.value;
    // get random id
    const id = Math.floor(Math.random() * 10);
    payload.id = id;
    this.paymentService.addPayment(payload);
  }

  cancel() {
    this.router.navigate(['/']);
  }

  ngOnDestroy() {
    this.sub2.unsubscribe();
    this.sub1.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { PaymentService } from 'src/app/_services/payment.service';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss'],
})
export class PaymentListComponent implements OnInit {
  constructor(private paymentService: PaymentService) {}

  ngOnInit(): void {}

  addPayment() {}
}

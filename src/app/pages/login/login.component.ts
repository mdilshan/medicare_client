import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { AuthService } from 'src/app/_services/auth-service.service';
import { AppContextService } from 'src/app/_services/app-context.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EMAIL_REGEX } from 'src/app/utils/Utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginFormGroup = new FormGroup({
    email: new FormControl(null, [
      Validators.required,
      Validators.pattern(EMAIL_REGEX),
    ]),
    password: new FormControl(null, [Validators.required]),
    remember: new FormControl(true, [Validators.required]),
  });
  remember_me = true;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  async ngOnInit() {
    const isLoggedIn = await this.authService.authCheck();
    if (isLoggedIn) {
      this.router.navigate(['/']);
    }
  }
  async login() {
    try {
      if (this.loginFormGroup.valid) {
        const value = this.loginFormGroup.value;

        const form = new FormData();
        form.append('username', value.email);
        form.append('password', value.password);
        const data = {
          username: value.email,
          password: value.password,
        };
        this.appContextService.setProgressbarValue(true);
        const response = await this.userService.loginUser(data);

        if (response && response.accessToken) {
          this.authService.authToken.next(response.accessToken);
          this.authService.authState.next(1);

          await this.authService.saveUsersLocalStorage(response.accessToken);
          this.router.navigate(['/']);
        } else {
          this.appContextService.showSnackBarShow(
            'Access Denied!',
            null,
            3000,
            'snackbar-red'
          );
        }
      } else {
        this.appContextService.showSnackBarShow(
          'Email/Password required',
          null,
          3000,
          'snackbar-red'
        );
      }
    } catch (err) {
      if (typeof err == 'string') {
        this.appContextService.showSnackBarShow(
          err,
          null,
          3000,
          'snackbar-red'
        );
      } else {
        this.appContextService.showSnackBarShow(
          'Email or Password not valid',
          null,
          3000,
          'snackbar-red'
        );
      }
    } finally {
      this.appContextService.setProgressbarValue(false);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { PaymentService } from 'src/app/_services/payment.service';
import { AppContextService } from 'src/app/_services/app-context.service';
import { IFullPayment } from 'src/app/DTO';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.scss'],
})
export class PaymentDetailsComponent implements OnInit {
  data: IFullPayment;
  constructor(
    private paymentService: PaymentService,
    private appContextService: AppContextService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['payment_id'];
    if (id) {
      this.init(id);
    } else {
      this.router.navigate(['/']);
    }
  }

  async init(id: number) {
    try {
      this.appContextService.setProgressbarValue(true);
      const res = await this.paymentService.getOnePayment(id);
      this.data = res.results[0];
    } catch (err) {
      this.appContextService.showSnackBarShow(err.message || err);
    } finally {
      this.appContextService.setProgressbarValue(false);
    }
  }

  onDelete() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '30%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      //console.log(result);
      if (result == 1) {
        this.paymentService.deletePayment(this.data.payment_id);
      }
    });
  }
}

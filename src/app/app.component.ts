import { Component, OnInit } from '@angular/core';
import { AppContextService } from './_services/app-context.service';
import { AuthService } from './_services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'payment';
  constructor(
    public appContextService: AppContextService,
    private authService: AuthService,
    private router: Router
  ) {}

  async ngOnInit() {
    const auth = await this.authService.authCheck();
    if (!auth) {
      this.router.navigate(['login']);
    }
  }
}

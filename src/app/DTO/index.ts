export interface IDoctor {
  id: number;
  fname: string;
  lname: string;
  special: string;
  experience: string;
  mobile: string;
  dob: number;
  gender: string;
  email: string;
  address: string;
  img: string;
  description: string;
}

export interface DocotrDto {
  results: IDoctor[];
}

export interface ICustomer {
  id: number;
  fname: string;
  lname: string;
  mobile: string;
  dob: number;
  gender: string;
  email: string;
  address: string;
  img: string;
}

export interface CustomerDto {
  results: ICustomer[];
}

export interface IPayment {
  payment_id: number;
  doct_id: number;
  cust_id: number;
  service: string;
  cost: string;
  discount: string;
  type: string;
  cust_name: string;
  cust_mobile: string;
  cust_gender: string;
  cust_email: string;
  doct_name: string;
  doct_special: string;
  doct_mobile: string;
  doct_gender: string;
  full_count: number;
}

export interface PaymentDto {
  results: IPayment[];
}

export interface IFullPayment {
  payment_id: number;
  doct_id: number;
  cust_id: number;
  service: string;
  cost: string;
  discount: string;
  type: string;
  cust_dob: number;
  cust_name: string;
  cust_mobile: string;
  cust_gender: string;
  cust_email: string;
  cust_address: string;
  cust_avatar: string;
  doct_name: string;
  doct_special: string;
  doct_mobile: string;
  doct_experience: string;
  doct_gender: string;
  doct_dob: string;
  doct_email: string;
  doct_desc: string;
  doct_avatar: string;
}

export interface FullPaymanetDetailsDTO {
  results: IFullPayment[];
}

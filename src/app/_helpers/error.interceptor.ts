import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { AuthService } from '../_services/auth-service.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppContextService } from '../_services/app-context.service';
import { Location } from '@angular/common';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private contextService: AppContextService,
    private location: Location
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((err) => {
        // logout if forbinded or unauthorized
        if ([401, 403].indexOf(err.status) !== -1) {
          this.contextService.showSnackBarShow('Not Authroized');
          this.location.back();
        }
        // else return the error message
        //console.log("err is", err);
        const error = err.error.message || err.statusText || err.message || err;
        this.contextService.showSnackBarShow(error);
        return throwError(error);
      })
    );
  }
}

import { AuthService } from '../_services/auth-service.service';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let currentUserToken: string = this.authService.getToken();
    if (currentUserToken) {
      //console.log("Trigger");
      const headers = new HttpHeaders({
        Authorization: 'Bearer ' + currentUserToken,
      });
      const cloned_req = req.clone({
        headers: headers,
      });

      return next.handle(cloned_req);
    }

    return next.handle(req);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentListComponent } from './pages/payment-list/payment-list.component';
import { PaymentFormComponent } from './pages/payment-form/payment-form.component';
import { LoginComponent } from './pages/login/login.component';
import { UserAuthGuard } from './_helpers/auth.guard';
import { PaymentDetailsComponent } from './pages/payment-details/payment-details.component';
import { EditPaymentComponent } from './pages/edit-payment/edit-payment.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    canActivate: [UserAuthGuard],
    component: PaymentListComponent,
  },
  {
    path: 'new',
    canActivate: [UserAuthGuard],
    component: PaymentFormComponent,
  },
  {
    path: 'show/:payment_id',
    canActivate: [UserAuthGuard],
    component: PaymentDetailsComponent,
  },
  {
    path: 'edit/:payment_id',
    canActivate: [UserAuthGuard],
    component: EditPaymentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

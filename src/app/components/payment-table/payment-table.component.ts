import { Component, OnInit } from '@angular/core';
import { IPayment } from 'src/app/DTO';
import { PaymentService } from 'src/app/_services/payment.service';

@Component({
  selector: 'app-payment-table',
  templateUrl: './payment-table.component.html',
  styleUrls: ['./payment-table.component.scss'],
})
export class PaymentTableComponent implements OnInit {
  displayedColumns: string[] = [
    'payment_id',
    'cust_name',
    'cust_mobile',
    'doct_name',
    'service',
    'cost',
  ];
  total: number = 0;
  fetched: number = 0;
  currentPage = 0;
  data: IPayment[] = [];
  isLoadingResults = false;
  constructor(private paymentService: PaymentService) {}

  ngOnInit(): void {
    this.paymentService.payments$.subscribe((payments) => {
      this.isLoadingResults = false;
      if (payments && payments.length > 0) {
        this.total = payments[0].full_count;
        this.fetched = this.data.length + payments.length;
        this.data = payments;
      }
    });
    this.isLoadingResults = true;
    this.paymentService.getPayments(10, 0);
  }

  getPayments() {
    const skip = this.fetched;
    const limit = 10;
    this.isLoadingResults = true;
    this.paymentService.getPayments(limit, skip);
  }

  addPayment() {}
}
